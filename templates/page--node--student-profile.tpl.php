<?php

/**
 * @file
 */
?>
<div id="site" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div id="skip">
    <a href="#main" class="element-invisible element-focusable"><?php print t('Skip to main'); ?></a>
    <a href="#footer" class="element-invisible element-focusable"><?php print t('Skip to footer'); ?></a>
  </div>
  <div id="header">
    <?php print render($page['global_header']); ?>
    <div id="site-header"><a href="<?php print $front_page ?>" title="<?php print $site_name; ?>" rel="home"><img src="<?php print $logo; ?>" alt="<?php print $site_name; ?>"/></a></div>
    <div id="site-navigation"><?php print render($page['sidebar_first']); ?></div>
  </div><!--/header-->
  <div id="main" class="clearfix">
    <?php print render($page['banner']); ?>
    <?php print $messages; ?>
    <?php print render($page['help']); ?>
    <?php print $breadcrumb; ?>
    <?php if ($tabs): ?><div class="node-tabs"><?php print render($tabs); ?></div><?php
    endif; ?>
    <div id="content">
      <?php
      print render($page['content']);
      ?>
    </div><!--/main-content-->
    <div id="site-sidebar">
      <?php  print render($page['sidebar_second']); ?>
    </div>
  </div><!--/main-->
  <div id="footer">
    <div id="watermark"></div>
    <?php if (!empty($page['site_footer'])): ?>
      <div id="site-footer" class="clearfix"><?php print render($page['site_footer']); ?></div>
    <?php endif; ?>
    <?php print render($page['global_footer']); ?>
    <?php print render($page['login_link']); ?>
  </div><!--/footer-->
</div><!--/site-->
