<?php

/**
 * @file
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>" <?php print $attributes; ?>>
  <div class="node-inner">

    <?php if ($page): ?>
      <h1<?php print $title_attributes; ?>>
      <?php
      print $title;
      ?>
      </h1>
    <?php else: ?>
      <h2<?php print $title_attributes; ?>>
      <a href="<?php print $node_url; ?>">
      <?php
      print render($content['field_photo']);
      print $title;
      ?>
      </a>
      </h2>
    <?php endif; ?>

    <div class="content_node"<?php print $content_attributes; ?>>
      <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      // Hide things that we are manually rendering.
      hide($content['field_hometown']);
      hide($content['field_country']);
      hide($content['field_faculty']);
      hide($content['field_academic_year']);
      hide($content['field_academic_year_type']);
      hide($content['field_program']);
      hide($content['field_student_type']);
      hide($content['field_question_why_chose']);
      hide($content['field_question_faculty_program']);
      hide($content['field_question_hobbies']);
      hide($content['field_question_campus_activities']);
      hide($content['field_question_interesting_facts']);
      hide($content['field_question_after_graduation']);
      hide($content['field_question_tips']);
      // Render those things.
      if ($page) {
        print render($content['field_photo']);
      }
      print '<div class="student-profiles-bio">';
      print '<p class="wherefrom">' . render($content['field_hometown'][0]['#markup']) . ', ' . render($content['field_country'][0]['#markup']) . '</p>';
      print '<p>' . render($content['field_faculty'][0]['#markup']) . '</p>';
      print '<p>' . render($content['field_academic_year'][0]['#markup']) . ' (' . render($content['field_academic_year_type'][0]['#markup']) . ')</p>';
      print '<p class="program">' . render($content['field_program'][0]['#markup']) . '</p>';
      print '</div>';
      // Render questions.
      $questions = array('field_question_why_chose', 'field_question_faculty_program', 'field_question_hobbies', 'field_question_campus_activities', 'field_question_interesting_facts', 'field_question_after_graduation', 'field_question_tips');
      if (!$page) {
        // If we're not on a page, only display one random question.
        $questions = array($questions[rand(0, count($questions) - 1)]);
      }
      print '<dl>';
      foreach ($questions as $question) {
        print '<dt>' . render($content[$question]['#title']) . '</dt>';
        print '<dd>' . render($content[$question][0]['#markup']) . '</dd>';
      }
      print '</dl>';
      // Render anything else.
      print render($content);
      ?>
      <?php if (!$page): ?>
      <p class="view-profile"><a href="<?php print $node_url; ?>">view the entire profile</a></p>
      <?php endif; ?>
    </div>

  </div> <!-- /node-inner -->
</div> <!-- /node-->

<?php print render($content['comments']); ?>
