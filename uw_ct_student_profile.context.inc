<?php

/**
 * @file
 * uw_ct_student_profile.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_student_profile_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'uw_student_profiles_homepage';
  $context->description = 'Display a random student profile on the home page';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-1e54fe8c8786b575e077a811d5a98b61' => array(
          'module' => 'views',
          'delta' => '1e54fe8c8786b575e077a811d5a98b61',
          'region' => 'sidebar_second',
          'weight' => '-30',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Display a random student profile on the home page');
  $export['uw_student_profiles_homepage'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'uw_student_profiles_profile';
  $context->description = 'Student profiles for the Waterloo International site';
  $context->tag = 'Content';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'student_profile' => 'student_profile',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_ct_student_profile-uw_student_profile' => array(
          'module' => 'uw_ct_student_profile',
          'delta' => 'uw_student_profile',
          'region' => 'sidebar_second',
          'weight' => '-30',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Student profiles for the Waterloo International site');
  $export['uw_student_profiles_profile'] = $context;

  return $export;
}
