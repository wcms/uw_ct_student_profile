<?php

/**
 * @file
 * uw_ct_student_profile.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function uw_ct_student_profile_default_rules_configuration() {
  $items = array();
  $items['rules_student_profile_change_status'] = entity_import('rules_config', '{ "rules_student_profile_change_status" : {
      "LABEL" : "Student profile change status (approved)",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "student profile" ],
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_update" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "student_profile" : "student_profile" } }
          }
        },
        { "NOT data_is" : { "data" : [ "node-unchanged:field-status" ], "value" : "approved" } },
        { "data_is" : { "data" : [ "node:field-status" ], "value" : "approved" } }
      ],
      "DO" : [ { "node_publish" : { "node" : [ "node" ] } } ]
    }
  }');
  $items['rules_student_profile_change_status_not_approved_'] = entity_import('rules_config', '{ "rules_student_profile_change_status_not_approved_" : {
      "LABEL" : "Student profile change status (not approved)",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "student profile" ],
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_update" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "student_profile" : "student_profile" } }
          }
        },
        { "data_is" : { "data" : [ "node-unchanged:field-status" ], "value" : "approved" } },
        { "NOT data_is" : { "data" : [ "node:field-status" ], "value" : "approved" } }
      ],
      "DO" : [ { "node_unpublish" : { "node" : [ "node" ] } } ]
    }
  }');
  $items['rules_student_profile_initial_submit_approved'] = entity_import('rules_config', '{ "rules_student_profile_initial_submit_approved" : {
      "LABEL" : "Student profile initial submit approved",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "student profile" ],
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_insert" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "student_profile" : "student_profile" } }
          }
        },
        { "data_is" : { "data" : [ "node:field-status" ], "value" : "approved" } }
      ],
      "DO" : [ { "node_publish" : { "node" : [ "node" ] } } ]
    }
  }');
  $items['rules_student_profile_initial_submit_email'] = entity_import('rules_config', '{ "rules_student_profile_initial_submit_email" : {
      "LABEL" : "Student profile initial submit email",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "student profile" ],
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_insert" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "student_profile" : "student_profile" } }
          }
        }
      ],
      "DO" : [
        { "mail_to_users_of_role" : {
            "roles" : { "value" : { "6" : "6", "5" : "5" } },
            "subject" : "A new student profile has been submitted for review",
            "message" : "A student has submitted a profile to [site:name]. Please login and review the submission."
          }
        }
      ]
    }
  }');
  $items['rules_student_profile_published'] = entity_import('rules_config', '{ "rules_student_profile_published" : {
      "LABEL" : "Student profile published",
      "PLUGIN" : "reaction rule",
      "WEIGHT" : "1",
      "TAGS" : [ "student profile" ],
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_update" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "student_profile" : "student_profile" } }
          }
        },
        { "node_is_published" : { "node" : [ "node" ] } },
        { "NOT data_is" : { "data" : [ "node:field-status" ], "value" : "approved" } }
      ],
      "DO" : [
        { "data_set" : { "data" : [ "node:field-status" ], "value" : "approved" } }
      ]
    }
  }');
  $items['rules_student_profile_submit'] = entity_import('rules_config', '{ "rules_student_profile_submit" : {
      "LABEL" : "Student profile initial submit",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "student profile" ],
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_insert" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "student_profile" : "student_profile" } }
          }
        }
      ],
      "DO" : [ { "redirect" : { "url" : "profile-submitted" } } ]
    }
  }');
  $items['rules_student_profile_unpublished'] = entity_import('rules_config', '{ "rules_student_profile_unpublished" : {
      "LABEL" : "Student profile unpublished",
      "PLUGIN" : "reaction rule",
      "WEIGHT" : "1",
      "TAGS" : [ "student profile" ],
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_update" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "student_profile" : "student_profile" } }
          }
        },
        { "NOT node_is_published" : { "node" : [ "node" ] } },
        { "NOT data_is" : {
            "data" : [ "node:field-status" ],
            "op" : "IN",
            "value" : { "value" : { "decision pending" : "decision pending", "declined" : "declined" } }
          }
        }
      ],
      "DO" : [
        { "data_set" : { "data" : [ "node:field-status" ], "value" : "declined" } }
      ]
    }
  }');
  return $items;
}
