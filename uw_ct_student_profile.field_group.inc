<?php

/**
 * @file
 * uw_ct_student_profile.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uw_ct_student_profile_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_approval|node|student_profile|form';
  $field_group->group_name = 'group_approval';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'student_profile';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Approval and Release',
    'weight' => '5',
    'children' => array(
      0 => 'field_permissions',
      1 => 'field_notify_by_email',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_approval|node|student_profile|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_personal_information|node|student_profile|form';
  $field_group->group_name = 'group_personal_information';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'student_profile';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Personal Information',
    'weight' => '0',
    'children' => array(
      0 => 'field_email_address',
      1 => 'field_hometown',
      2 => 'field_faculty',
      3 => 'field_academic_year',
      4 => 'field_academic_year_type',
      5 => 'field_program',
      6 => 'field_country',
      7 => 'title',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Personal Information',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => 'You are not required to include your last name.',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $export['group_personal_information|node|student_profile|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_photo|node|student_profile|form';
  $field_group->group_name = 'group_photo';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'student_profile';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Upload Your Photo',
    'weight' => '4',
    'children' => array(
      0 => 'field_photo',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_photo|node|student_profile|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_profile_questions|node|student_profile|form';
  $field_group->group_name = 'group_profile_questions';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'student_profile';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Profile Questions',
    'weight' => '3',
    'children' => array(
      0 => 'field_question_why_chose',
      1 => 'field_question_faculty_program',
      2 => 'field_question_hobbies',
      3 => 'field_question_campus_activities',
      4 => 'field_question_interesting_facts',
      5 => 'field_question_after_graduation',
      6 => 'field_question_tips',
      7 => 'field_student_type',
      8 => 'field_student_type_other',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_profile_questions|node|student_profile|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_status|node|student_profile|form';
  $field_group->group_name = 'group_status';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'student_profile';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Profile Status/Actions',
    'weight' => '6',
    'children' => array(
      0 => 'field_status',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_status|node|student_profile|form'] = $field_group;

  return $export;
}
