<?php

/**
 * @file
 * uw_ct_student_profile.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_student_profile_user_default_permissions() {
  $permissions = array();

  // Exported permission: create field_status.
  $permissions['create field_status'] = array(
    'name' => 'create field_status',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: create student_profile content.
  $permissions['create student_profile content'] = array(
    'name' => 'create student_profile content',
    'roles' => array(
      0 => 'administrator',
      1 => 'anonymous user',
      2 => 'authenticated user',
      3 => 'content author',
      4 => 'content editor',
      5 => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any student_profile content.
  $permissions['delete any student_profile content'] = array(
    'name' => 'delete any student_profile content',
    'roles' => array(
      0 => 'administrator',
      1 => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own student_profile content.
  $permissions['delete own student_profile content'] = array(
    'name' => 'delete own student_profile content',
    'roles' => array(
      0 => 'administrator',
      1 => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any student_profile content.
  $permissions['edit any student_profile content'] = array(
    'name' => 'edit any student_profile content',
    'roles' => array(
      0 => 'administrator',
      1 => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: edit field_status.
  $permissions['edit field_status'] = array(
    'name' => 'edit field_status',
    'roles' => array(
      0 => 'administrator',
      1 => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own field_status.
  $permissions['edit own field_status'] = array(
    'name' => 'edit own field_status',
    'roles' => array(
      0 => 'administrator',
      1 => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own student_profile content.
  $permissions['edit own student_profile content'] = array(
    'name' => 'edit own student_profile content',
    'roles' => array(
      0 => 'administrator',
      1 => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: enter student_profile revision log entry.
  $permissions['enter student_profile revision log entry'] = array(
    'name' => 'enter student_profile revision log entry',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: override student_profile authored by option.
  $permissions['override student_profile authored by option'] = array(
    'name' => 'override student_profile authored by option',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: override student_profile authored on option.
  $permissions['override student_profile authored on option'] = array(
    'name' => 'override student_profile authored on option',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: override student_profile promote to front page option.
  $permissions['override student_profile promote to front page option'] = array(
    'name' => 'override student_profile promote to front page option',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: override student_profile published option.
  $permissions['override student_profile published option'] = array(
    'name' => 'override student_profile published option',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: override student_profile revision option.
  $permissions['override student_profile revision option'] = array(
    'name' => 'override student_profile revision option',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: override student_profile sticky option.
  $permissions['override student_profile sticky option'] = array(
    'name' => 'override student_profile sticky option',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: view field_status.
  $permissions['view field_status'] = array(
    'name' => 'view field_status',
    'roles' => array(
      0 => 'administrator',
      1 => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_status.
  $permissions['view own field_status'] = array(
    'name' => 'view own field_status',
    'roles' => array(
      0 => 'administrator',
      1 => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  return $permissions;
}
